#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xstatus.h"
#include "xparameters.h"
#include "p128_rb_ip.h"
#include "xil_io.h"
#include "present128_hw.h"

int p128_IsrExecuted;
uint32_t *result_ptr;
int num_blocks;

///////////////////////////////////////////
// PRESENT-128 Interrupt Service Routine //
///////////////////////////////////////////

void p128_hw_isr() {

	// Read ciphertext from BRAM
	for (int i = 0; i < num_blocks; i++) {
		result_ptr[i*2]     = Xil_In32(XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR + (4 + (i*8)));  // Bits [63:32]
		result_ptr[(i*2)+1] = Xil_In32(XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR + (i*8));        // Bits [31:0]
	}
	p128_IsrExecuted = 1;

}

/////////////////////////////////////////
// PRESENT-128 hardware initialization //
/////////////////////////////////////////

void p128_hw_init(uint32_t *key, XScuGic *p128_intc){

	p128_IsrExecuted = 0;

	XScuGic *intc_ptr = p128_intc;
	XScuGic_Config *intc_cfg;

	// Get config for PS General Interrupt Controller (GIC)
	intc_cfg = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);

	// Initialize GIC driver
	XScuGic_CfgInitialize(intc_ptr, intc_cfg, intc_cfg->CpuBaseAddress);

	// Set highest priority (0xA0) and rising edge triggered (0x03)
	XScuGic_SetPriorityTriggerType(intc_ptr, XPAR_FABRIC_P128_RB_IP_0_P128_DONE_INTR_INTR, 0xA0, 0x03);

	// Register the Interrupt Service Routine (ISR) (check last argument)
	XScuGic_Connect(intc_ptr, XPAR_FABRIC_P128_RB_IP_0_P128_DONE_INTR_INTR, (Xil_ExceptionHandler)p128_hw_isr, (void *)intc_ptr);

	// Enable the relevant GIC interrupt
	XScuGic_Enable(intc_ptr, XPAR_FABRIC_P128_RB_IP_0_P128_DONE_INTR_INTR);

	// Initialize exception table, register ISR and enable exceptions
	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, intc_ptr);
	Xil_ExceptionEnable();

	// Write key to P128_KEY[3:0] registers
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG4_OFFSET, key[0]);  // Bits [127:96]
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG5_OFFSET, key[1]);  // Bits [95:64]
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG6_OFFSET, key[2]);  // Bits [63:32]
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG7_OFFSET, key[3]);  // Bits [31:0]

}

/////////////////////////////////////////////
// PRESENT-128 hardware encryption routine //
/////////////////////////////////////////////

void p128_hw_encrypt(uint32_t *plaintext, uint32_t *ciphertext, int length){

	// Write plaintext to BRAM
	for (int i = 0; i < length; i++) {
		Xil_Out32(XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR + (4 + (i*8)), plaintext[i*2]);  // Bits [63:32]
		Xil_Out32(XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR + (i*8), plaintext[(i*2)+1]);  // Bits [31:0]
	}

	// Set length and trigger "go" condition (via write to P128_CFG register)
	uint32_t cfg_val = ((length << 1) & 0xFFE) | 0x1;
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG0_OFFSET, cfg_val);


	result_ptr = ciphertext;
	num_blocks = length;

	// De-assert "go" bit
	Xil_Out32(XPAR_P128_RB_IP_0_S00_AXI_BASEADDR + P128_RB_IP_S00_AXI_SLV_REG0_OFFSET, (cfg_val & 0xFFFFFFFE));

}

