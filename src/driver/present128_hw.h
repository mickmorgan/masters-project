#ifndef SRC_PRESENT128_HW_H_
#define SRC_PRESENT128_HW_H_

#include "xscugic.h"

extern int p128_IsrExecuted;
extern uint32_t *result_ptr;

void p128_hw_isr();
void p128_hw_init(uint32_t *key, XScuGic *p128_intc);
void p128_hw_encrypt(uint32_t *plaintext, uint32_t *ciphertext, int length);

#endif /* SRC_PRESENT128_HW_H_ */
