
#ifndef SRC_PRESENT128_SW_H_
#define SRC_PRESENT128_SW_H_

void present128_encrypt(uint32_t *plaintext, uint32_t *key, uint32_t *result, int length);

#endif /* SRC_PRESENT128_SW_H_ */
