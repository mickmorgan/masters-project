#include <stdio.h>
#include "platform.h"
#include "xil_io.h"

// PRESENT S-box LUT
static const uint32_t sbox[16] = {
		0x0C, 0x05, 0x06, 0x0B, 0x09, 0x00, 0x0A, 0x0D, 0x03, 0x0E, 0x0F, 0x08, 0x04, 0x07, 0x01, 0x02
};

// Main encryption function
void present128_encrypt(uint32_t *plaintext, uint32_t *key, uint32_t *result, int length) {

	// Initialize variables
	uint32_t round_counter;
	uint32_t state[2];
	uint32_t round_key[4] = {key[0], key[1], key[2], key[3]};

	// For each block
	for (int i = 0; i < length; i++) {

		state[0] = plaintext[i*2];
		state[1] = plaintext[(i*2) + 1];

		// 31 rounds of PRESENT algorithm
		for (round_counter = 1; round_counter <= 31; round_counter++) {
			_present128_addRoundKey(&state, &round_key);
			_present128_sBoxLayer(&state);
			_present128_pLayer(&state);
			_present128_updateRoundKey(&round_key, round_counter);
		}

		// Final whitening
		_present128_addRoundKey(&state, &round_key);

		result[i*2] = state[0];
		result[(i*2) + 1] = state[1];
	}

}


// Update round key
void _present128_updateRoundKey(uint32_t *round_key, uint32_t round_counter) {

	// Rotate key left by 61 bits
	_present128_rotl61(round_key);

	// Pass upper 8 bits through S-box
	round_key[0] = (sbox[(round_key[0] >> 28) & 0x0000000F] << 28) |
			       (sbox[(round_key[0] >> 24) & 0x0000000F] << 24) |
				   (round_key[0] & 0x00FFFFFF);

	// XOR bits [66:62] with round counter
	round_key[1] = ((round_counter >>  2) ^ (round_key[1] & 0x7)) | (round_key[1] & 0xFFFFFFF8);
	round_key[2] = ((round_counter << 30) ^ (round_key[2] & 0xC0000000)) | (round_key[2] & 0x3FFFFFFF);

}


// Substitute 64-bit state through 16x4 S-boxes
void _present128_sBoxLayer(uint32_t *state) {

	state[0] = (sbox[(state[0] >> 28) & 0x0000000F] << 28) |
			   (sbox[(state[0] >> 24) & 0x0000000F] << 24) |
			   (sbox[(state[0] >> 20) & 0x0000000F] << 20) |
			   (sbox[(state[0] >> 16) & 0x0000000F] << 16) |
			   (sbox[(state[0] >> 12) & 0x0000000F] << 12) |
			   (sbox[(state[0] >> 8) & 0x0000000F] << 8) |
			   (sbox[(state[0] >> 4) & 0x0000000F] << 4) |
			   (sbox[(state[0]) & 0x0000000F]);

	state[1] = (sbox[(state[1] >> 28) & 0x0000000F] << 28) |
			   (sbox[(state[1] >> 24) & 0x0000000F] << 24) |
			   (sbox[(state[1] >> 20) & 0x0000000F] << 20) |
			   (sbox[(state[1] >> 16) & 0x0000000F] << 16) |
			   (sbox[(state[1] >> 12) & 0x0000000F] << 12) |
			   (sbox[(state[1] >> 8) & 0x0000000F] << 8) |
			   (sbox[(state[1] >> 4) & 0x0000000F] << 4) |
			   (sbox[(state[1]) & 0x0000000F]);

}


// PRESENT permutation layer (64-bit permutation)
//
// tempState[] represents the input vector for the permutation operation. 
// Each bit of the output vector (state) is assigned by shifting the input vector
// (tempState) such that the correct bit index is placed in the correct position.
// Then that bit is masked using an "&" operator, and the entire output vector
// is constructed by "ORing" each masked 32-bit component.
//
// For example, the PRESENT spec defines the mapping input[51] -> output[60].
// To place bit 51 of the input "tempState" vector in the 60th bit position of the
// output "state" vector, we shift bit 19 of "tempState[0]" left by 9 bit positions
// to place it in the 28th bit position of "state[0]".

void _present128_pLayer(uint32_t *state) {

	uint32_t tempState[2] = {state[0], state[1]};

	state[0] = ((tempState[0]      ) & (1 << 31)) |  // Bit 63
			   ((tempState[0] <<  3) & (1 << 30)) |  // Bit 62
			   ((tempState[0] <<  6) & (1 << 29)) |  // Bit 61
			   ((tempState[0] <<  9) & (1 << 28)) |  // Bit 60
			   ((tempState[0] << 12) & (1 << 27)) |  // Bit 59
			   ((tempState[0] << 15) & (1 << 26)) |  // Bit 58
			   ((tempState[0] << 18) & (1 << 25)) |  // Bit 57
			   ((tempState[0] << 21) & (1 << 24)) |  // Bit 56
			   ((tempState[1] >>  8) & (1 << 23)) |  // Bit 55
			   ((tempState[1] >>  5) & (1 << 22)) |  // Bit 54
			   ((tempState[1] >>  2) & (1 << 21)) |  // Bit 53
			   ((tempState[1] <<  1) & (1 << 20)) |  // Bit 52
			   ((tempState[1] <<  4) & (1 << 19)) |  // Bit 51
			   ((tempState[1] <<  7) & (1 << 18)) |  // Bit 50
			   ((tempState[1] << 10) & (1 << 17)) |  // Bit 49
			   ((tempState[1] << 13) & (1 << 16)) |  // Bit 48
			   ((tempState[0] >> 15) & (1 << 15)) |  // Bit 47
	   	   	   ((tempState[0] >> 12) & (1 << 14)) |  // Bit 46
			   ((tempState[0] >>  9) & (1 << 13)) |  // Bit 45
	           ((tempState[0] >>  6) & (1 << 12)) |  // Bit 44
	   	   	   ((tempState[0] >>  3) & (1 << 11)) |  // Bit 43
	   	   	   ((tempState[0]      ) & (1 << 10)) |  // Bit 42
			   ((tempState[0] <<  3) & (1 <<  9)) |  // Bit 41
			   ((tempState[0] <<  6) & (1 <<  8)) |  // Bit 40
   	   	   	   ((tempState[1] >> 23) & (1 <<  7)) |  // Bit 39
   	   	   	   ((tempState[1] >> 20) & (1 <<  6)) |  // Bit 38
			   ((tempState[1] >> 17) & (1 <<  5)) |  // Bit 37
			   ((tempState[1] >> 14) & (1 <<  4)) |  // Bit 36
   	   	   	   ((tempState[1] >> 11) & (1 <<  3)) |  // Bit 35
   	   	   	   ((tempState[1] >>  8) & (1 <<  2)) |  // Bit 34
			   ((tempState[1] >>  5) & (1 <<  1)) |  // Bit 33
			   ((tempState[1] >>  2) & (1 <<  0)) ;  // Bit 32

	state[1] = ((tempState[0] <<  2) & (1 << 31)) |  // Bit 31
			   ((tempState[0] <<  5) & (1 << 30)) |  // Bit 30
			   ((tempState[0] <<  8) & (1 << 29)) |  // Bit 29
			   ((tempState[0] << 11) & (1 << 28)) |  // Bit 28
			   ((tempState[0] << 14) & (1 << 27)) |  // Bit 27
			   ((tempState[0] << 17) & (1 << 26)) |  // Bit 26
			   ((tempState[0] << 20) & (1 << 25)) |  // Bit 25
			   ((tempState[0] << 23) & (1 << 24)) |  // Bit 24
	   	   	   ((tempState[1] >>  6) & (1 << 23)) |  // Bit 23
	   	   	   ((tempState[1] >>  3) & (1 << 22)) |  // Bit 22
			   ((tempState[1]      ) & (1 << 21)) |  // Bit 21
			   ((tempState[1] <<  3) & (1 << 20)) |  // Bit 20
   	   	   	   ((tempState[1] <<  6) & (1 << 19)) |  // Bit 19
   	   	   	   ((tempState[1] <<  9) & (1 << 18)) |  // Bit 18
			   ((tempState[1] << 12) & (1 << 17)) |  // Bit 17
			   ((tempState[1] << 15) & (1 << 16)) |  // Bit 16
	   	   	   ((tempState[0] >> 13) & (1 << 15)) |  // Bit 15
	   	   	   ((tempState[0] >> 10) & (1 << 14)) |  // Bit 14
			   ((tempState[0] >>  7) & (1 << 13)) |  // Bit 13
			   ((tempState[0] >>  4) & (1 << 12)) |  // Bit 12
   	   	   	   ((tempState[0] >>  1) & (1 << 11)) |  // Bit 11
   	   	   	   ((tempState[0] <<  2) & (1 << 10)) |  // Bit 10
			   ((tempState[0] <<  5) & (1 <<  9)) |  // Bit  9
			   ((tempState[0] <<  8) & (1 <<  8)) |  // Bit  8
	   	   	   ((tempState[1] >> 21) & (1 <<  7)) |  // Bit  7
	   	   	   ((tempState[1] >> 18) & (1 <<  6)) |  // Bit  6
			   ((tempState[1] >> 15) & (1 <<  5)) |  // Bit  5
			   ((tempState[1] >> 12) & (1 <<  4)) |  // Bit  4
			   ((tempState[1] >>  9) & (1 <<  3)) |  // Bit  3
			   ((tempState[1] >>  6) & (1 <<  2)) |  // Bit  2
			   ((tempState[1] >>  3) & (1 <<  1)) |  // Bit  1
			   ((tempState[1]      ) & (1 <<  0)) ;  // Bit  0

}


// XOR round key with state
void _present128_addRoundKey(uint32_t *state, uint32_t *round_key) {

	state[0] = state[0] ^ round_key[0];
	state[1] = state[1] ^ round_key[1];

}


// Rotate 128-bit vector 61 places to the left
void _present128_rotl61(uint32_t *vector) {

	uint32_t temp[4] = {vector[0], vector[1], vector[2], vector[3]};
	vector[0] = temp[1] << 29 | temp[2] >> 3;
	vector[1] = temp[2] << 29 | temp[3] >> 3;
	vector[2] = temp[3] << 29 | temp[0] >> 3;
	vector[3] = temp[0] << 29 | temp[1] >> 3;

}
