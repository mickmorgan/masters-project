#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "present128_sw.h"
#include "present128_hw.h"
#include "xscugic.h"
#include "xstatus.h"
#include "xtime_l.h"


XStatus software_test(uint32_t *plaintext, uint32_t *key, uint32_t *expected, uint32_t *result) {

	xil_printf("Stimulus:\r\n");
	xil_printf("\tPlaintext:\t\t\t\t0x%08x %08x\r\n", plaintext[0], plaintext[1]);
	xil_printf("\tKey:\t\t\t\t\t\t\t0x%08x %08x %08x %08x\r\n", key[0], key[1], key[2], key[3]);
	xil_printf("Encrypting plaintext...\r\n");

	present128_encrypt(plaintext, key, result, 1);

	xil_printf("\tExpected result:\t0x%08x %08x\r\n", expected[0], expected[1]);
	xil_printf("\tActual result:\t\t0x%08x %08x\r\n", result[0], result[1]);

	if ((expected[0] == result[0]) & (expected[1] == result[1])){
		return XST_SUCCESS;
	} else {
		return XST_FAILURE;
	}

}

XStatus hardware_test(int length, uint32_t *plaintext, uint32_t *key, uint32_t *expected, uint32_t *result, XScuGic *p128_intc) {

	xil_printf("Stimulus:\r\n");
	xil_printf("\tPlaintext:\t\t\t\t0x%08x %08x\r\n", plaintext[0], plaintext[1]);
	xil_printf("\tKey:\t\t\t\t\t\t\t0x%08x %08x %08x %08x\r\n", key[0], key[1], key[2], key[3]);

	xil_printf("Writing key to hardware...\r\n");
	xil_printf("Setting up interrupt...\r\n");
	p128_hw_init(key, p128_intc);

	xil_printf("Encrypting Plaintext...\r\n");
	p128_hw_encrypt(plaintext, result, length);

	// Block until ISR executes
	xil_printf("Waiting for interrupt...\r\n");
	while(p128_IsrExecuted == 0){}

	xil_printf("Interrupt triggered!\r\n");
	p128_IsrExecuted = 0;
	xil_printf("\tExpected result:\t0x%08x %08x\r\n", expected[0], expected[1]);
	xil_printf("\tActual result:\t\t0x%08x %08x\r\n", result[0], result[1]);

	if ((expected[0] == result[0]) & (expected[1] == result[1])){
		return XST_SUCCESS;
	} else {
		return XST_FAILURE;
	}

}

XStatus performance_test(XScuGic *p128_intc){

	// Randomly initialized data - contents don't matter
	uint32_t plaintext[1024][2];
	uint32_t ciphertext[1024][2];
	uint32_t key[4];

	// Timer variables
	XTime start, end;
	int t;

	printf("Start Software Performance Test\n");

	// Single block
	XTime_GetTime(&start);
	present128_encrypt(plaintext, key, ciphertext, 1);
	XTime_GetTime(&end);
	t = (end - start)/(COUNTS_PER_SECOND/1000000);

	// 1024 blocks
	XTime_GetTime(&start);
	present128_encrypt(plaintext, key, ciphertext, 1024);
	XTime_GetTime(&end);
	t = (end - start)/(COUNTS_PER_SECOND/1000000);
	printf("   Time to encrypt 1024 blocks = %d us \n", t);

	printf("Start Hardware Performance Test\n");

	// Single block
	XTime_GetTime(&start);
	p128_hw_init(&key, p128_intc);
	p128_hw_encrypt(&plaintext, &ciphertext, 1);
	while(p128_IsrExecuted == 0){}
	p128_IsrExecuted = 0;
	XTime_GetTime(&end);
	t = (end - start)/(COUNTS_PER_SECOND/1000000);
	printf("   Time to encrypt 1 block = %d us ()\n", t);

	// 1024 blocks
	XTime_GetTime(&start);
	p128_hw_init(&key, p128_intc);
	p128_hw_encrypt(&plaintext, &ciphertext, 1024);
	while(p128_IsrExecuted == 0){}
	p128_IsrExecuted = 0;
	XTime_GetTime(&end);
	t = (end - start)/(COUNTS_PER_SECOND/1000000);
	printf("   Time to encrypt 1024 blocks = %d us \n", t);

	return XST_SUCCESS;

}

int main(){

	init_platform();

	//Test Data
	uint32_t plaintext[2]  = {0x9CEFB24C, 0x4E51373D};
	uint32_t key[4]        = {0x2BD6459F, 0x82C5B300, 0x952C4910, 0x4881FF48};
	uint32_t expected[2]   = {0xEA024714, 0xAD5C4D84};
	uint32_t result[2];

	// 1. Execute software encryption test
	XStatus rc = software_test(plaintext, key, expected, result);

	// 2. Execute hardware encryption test
	//static XScuGic p128_intc;
	//XStatus rc = hardware_test(1, plaintext, key, expected, result, &p128_intc);

	// 3. Execute performance profiling test
	//static XScuGic p128_intc;
	//XStatus rc = performance_test(&p128_intc);

	if (rc == XST_SUCCESS) {
		xil_printf("\r\nP A S S\r\n");
	} else {
		xil_printf("\r\nF A I L\r\n");
	}

	cleanup_platform();

	return 0;

}


