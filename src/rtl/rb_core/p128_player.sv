// Module: p128_player
// Description : PRESENT-128 64-bit permutation layer
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_player (

    input logic [63:0] player_in,
    output logic [63:0] player_out

);

assign player_out[0] = player_in[0];
assign player_out[1] = player_in[4];
assign player_out[2] = player_in[8];
assign player_out[3] = player_in[12];
assign player_out[4] = player_in[16];
assign player_out[5] = player_in[20];
assign player_out[6] = player_in[24];
assign player_out[7] = player_in[28];
assign player_out[8] = player_in[32];
assign player_out[9] = player_in[36];
assign player_out[10] = player_in[40];
assign player_out[11] = player_in[44];
assign player_out[12] = player_in[48];
assign player_out[13] = player_in[52];
assign player_out[14] = player_in[56];
assign player_out[15] = player_in[60];
assign player_out[16] = player_in[1];
assign player_out[17] = player_in[5];
assign player_out[18] = player_in[9];
assign player_out[19] = player_in[13];
assign player_out[20] = player_in[17];
assign player_out[21] = player_in[21];
assign player_out[22] = player_in[25];
assign player_out[23] = player_in[29];
assign player_out[24] = player_in[33];
assign player_out[25] = player_in[37];
assign player_out[26] = player_in[41];
assign player_out[27] = player_in[45];
assign player_out[28] = player_in[49];
assign player_out[29] = player_in[53];
assign player_out[30] = player_in[57];
assign player_out[31] = player_in[61];
assign player_out[32] = player_in[2];
assign player_out[33] = player_in[6];
assign player_out[34] = player_in[10];
assign player_out[35] = player_in[14];
assign player_out[36] = player_in[18];
assign player_out[37] = player_in[22];
assign player_out[38] = player_in[26];
assign player_out[39] = player_in[30];
assign player_out[40] = player_in[34];
assign player_out[41] = player_in[38];
assign player_out[42] = player_in[42];
assign player_out[43] = player_in[46];
assign player_out[44] = player_in[50];
assign player_out[45] = player_in[54];
assign player_out[46] = player_in[58];
assign player_out[47] = player_in[62];
assign player_out[48] = player_in[3];
assign player_out[49] = player_in[7];
assign player_out[50] = player_in[11];
assign player_out[51] = player_in[15];
assign player_out[52] = player_in[19];
assign player_out[53] = player_in[23];
assign player_out[54] = player_in[27];
assign player_out[55] = player_in[31];
assign player_out[56] = player_in[35];
assign player_out[57] = player_in[39];
assign player_out[58] = player_in[43];
assign player_out[59] = player_in[47];
assign player_out[60] = player_in[51];
assign player_out[61] = player_in[55];
assign player_out[62] = player_in[59];
assign player_out[63] = player_in[63];

endmodule
