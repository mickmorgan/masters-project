// Module: p128_key_schedule
// Description : PRESENT-128 key schedule
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_key_schedule (

    input  logic [127:0] current_key,
    output logic [127:0] next_key,
    input  logic [4:0]   round_counter
    
);

wire [127:0] rotated_key;

// Rotate key 61 positions to the left
assign rotated_key = {current_key[66:0], current_key[127:67]};

// Left-most 8 bits passed through S-boxes
p128_sbox sbox_1 (
    .sbox_in  (rotated_key[127:124]),
    .sbox_out (next_key[127:124])
);

p128_sbox sbox_2 (
    .sbox_in  (rotated_key[123:120]),
    .sbox_out (next_key[123:120])
);

// XOR bits [66:62] of rotated key with round counter
assign next_key[66:62] = rotated_key[66:62] ^ round_counter;

// Pass remaining bits straight through
assign next_key[119:67] = rotated_key[119:67];
assign next_key[61:0] = rotated_key[61:0];

endmodule
