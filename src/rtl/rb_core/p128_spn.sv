// Module: p128_spn
// Description : PRESENT-128 substitution-permutation network
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_spn (

    input  logic [63:0] spn_in,
    output logic [63:0] spn_out

);

localparam SBOX_WIDTH = 4;
localparam NUM_SBOX = 16;

reg [63:0] post_sbox;

// 16 x 4-bit Substitutions (S-Box)
genvar i;
for (i = 0; i < NUM_SBOX; i = i + 1) begin : sbox
    p128_sbox i_sbox (
        .sbox_in  (spn_in[((SBOX_WIDTH) * (i + 1) - 1):(SBOX_WIDTH * i)]),
        .sbox_out (post_sbox[((SBOX_WIDTH) * (i + 1) - 1):(SBOX_WIDTH * i)])
    );
end

// 64-bit Permutation (P-Layer)
p128_player i_player (
    .player_in  (post_sbox),
    .player_out (spn_out)
);

endmodule
