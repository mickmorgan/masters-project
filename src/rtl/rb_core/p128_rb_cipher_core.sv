// Module: p128_rb_cipher_core
// Description : PRESENT-128 cipher core (round-based architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_rb_cipher_core (

    input  logic           clk,         // System clock
    input  logic           rst_b,       // System reset (active low)
    input  logic           load,        // Load plaintext and key
    input  logic [63:0]    plaintext,   // 64-bit plaintext block
    input  logic [127:0]   key,         // 128-bit encryption key
    input  logic [4:0]     count,       // Round counter from FSM
    output logic [63:0]    ciphertext   // 64-bit ciphertext block
    
);

reg [63:0]  state;
reg [127:0] round_key;
wire[127:0] next_round_key;
wire[63:0]  spn_out;

// State and Round Key Registers
always @ (posedge clk) begin
    if (!rst_b) begin
        state <= '0;
        round_key <= '0;
    end else begin
        if (load) begin
            state <= plaintext;
            round_key <= key;  
        end else begin
            state <= spn_out;
            round_key <= next_round_key;
        end
    end
end

// Substitution Permutation Network (SPN)
p128_spn i_spn (
    .spn_in  (ciphertext),
    .spn_out (spn_out)
);

// Key Schedule
p128_key_schedule i_key_schedule (
    .current_key   (round_key),
    .next_key      (next_round_key),
    .round_counter (count)
);

// Compute ciphertext
assign ciphertext = state ^ round_key[127:64];

endmodule