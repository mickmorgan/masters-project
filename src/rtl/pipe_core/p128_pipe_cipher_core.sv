// Module: p128_pipe_cipher_core
// Description : PRESENT-128 cipher core (pipelined architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_pipe_cipher_core (

    input  logic           clk,         // System clock
    input  logic           rst_b,       // System reset (active low)
    input  logic [63:0]    plaintext,   // 64-bit plaintext block
    input  logic [127:0]   key,         // 128-bit encryption key
    output logic [63:0]    ciphertext   // 64-bit ciphertext block

);

localparam NUM_STAGES = 31;                      // Number of pipeline stages

wire [63:0]  state_w         [0:NUM_STAGES];     // State wiring between pipeline stages
wire [127:0] key_w           [0:NUM_STAGES];     // Key wiring between pipeline stages

// Connect input ports to first pipeline stage
assign state_w[0] = plaintext;
assign key_w[0] = key;

// Instantiate 31 pipeline stages
generate
    genvar i;
    for (i=0; i<NUM_STAGES; i=i+1) begin
        p128_pipe_stage i_pipe_stage (
            .clk           (clk),
            .rst_b         (rst_b),
            .round_counter (i+1),                // Note: Static tie-off {5'b00001 to 5'b11111}
            .key_in        (key_w[i]),
            .key_out       (key_w[i+1]),
            .state_in      (state_w[i]),
            .state_out     (state_w[i+1])
        );
    end
endgenerate

// Compute ciphertext (post-whitening)
assign ciphertext = state_w[NUM_STAGES] ^ key_w[NUM_STAGES][127:64];

endmodule
