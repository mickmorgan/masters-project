// Module: p128_pipe_stage
// Description : PRESENT-128 pipeline stage (pipelined architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_pipe_stage (
    
    input  logic         clk,
    input  logic         rst_b,
    input  logic [4:0]   round_counter,
    input  logic [127:0] key_in,
    output logic [127:0] key_out,
    input  logic [63:0]  state_in,
    output logic [63:0]  state_out
    
);

wire [63:0] spn_in_w;
wire [63:0] spn_out_w;

// Key whitening
assign spn_in_w = state_in ^ key_in[127:64];

// Key Schedule
p128_key_schedule i_key_schedule (
    .current_key   (key_in),
    .next_key      (key_out),
    .round_counter (round_counter)
);

// Substitution Permutation Network (SPN)
p128_spn i_spn (
    .spn_in  (spn_in_w),
    .spn_out (spn_out_w)
); 

// Register state output
always @ (posedge clk) begin
    if (!rst_b) begin
        state_out <= '0;
    end else begin
        state_out <= spn_out_w;
    end 
end

endmodule
