// Module: p128_rb_ctrl
// Description : PRESENT-128 controller (Round-based architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_rb_ctrl (

    // System + register inputs
    input  logic           clk,        // System clock
    input  logic           rst_b,      // System reset (active low)
    input  logic           go_pulse,   // From P128_CFG configuration register
    input  logic [10:0]    num_blocks, // From P128_CFG configuration register (check width)
    output logic           done_intr,  // Edge triggered interrupt to indicate encryption completion
    input  logic [127:0]   key,
    
    // Plaintext block RAM interface
    output logic           pt_clka,     // BRAM clock
    output logic           pt_rsta,     // Optional output latch/register reset
    output logic [31:0]    pt_addra,    // Address for read + write operations (check width)
    output logic [63:0]    pt_wr_data,  // Write data (check width)
    input  logic [63:0]    pt_rd_data,  // Read data (check width)
    output logic           pt_ena,      // Optional clock enable (check if it's present on block RAM)
    output logic [7:0]     pt_wea,      // Byte write enable (tie low?)

    // Ciphertext block RAM interface
    output logic           ct_clka,     // BRAM clock
    output logic           ct_rsta,     // Optional output latch/register reset
    output logic [31:0]    ct_addra,    // Address for read + write operations (check width)
    output logic [63:0]    ct_wr_data,  // Write data (check width)
    input  logic [63:0]    ct_rd_data,  // Read data (check width)
    output logic           ct_ena,      // Optional clock enable (check if it's present on block RAM)
    output logic [7:0]     ct_wea,      // Byte write enable
    
    // Cipher core interface
    output logic           core_load,
    output logic [63:0]    core_plaintext,
    output logic [127:0]   core_key,
    output logic [4:0]     core_count,
    input  logic [63:0]    core_ciphertext

);

// Define FSM states
localparam STATE_INIT        = 3'b000;
localparam STATE_READ_BLOCK  = 3'b001;
localparam STATE_LOAD_BLOCK  = 3'b010;
localparam STATE_BUSY        = 3'b011;
localparam STATE_WRITE_BLOCK = 3'b100;
localparam STATE_DONE        = 3'b101;
localparam STATE_RSVD_1      = 3'b110;
localparam STATE_RSVD_2      = 3'b111;

// Declare variables for state and count
reg [2:0]  current_state;
reg [2:0]  next_state;
reg [4:0]  cnt;
reg [10:0] current_block;
reg [31:0] current_addr;
reg        enable_counter;
reg        incr_block_count;

// Direct assignments to cipher core
assign core_count     = cnt;
assign core_key       = key;        // May have to register this for timing
assign core_plaintext = pt_rd_data;

// Direct assignments to Plaintext BRAM
assign pt_clka    = clk;        // System clock
assign pt_rsta    = ~rst_b;     // Active high BRAM output register reset
assign pt_wr_data = '0;         // Tie off write data (won't be writing to this BRAM)
assign pt_ena     = 1'b1;       // Internal BRAM clock gating disabled
assign pt_wea     = '0;         // Tie off write enable (won't be writing to this BRAM)

// Direct assignments to Ciphertext BRAM
assign ct_clka    = clk;        // System clock
assign ct_rsta    = ~rst_b;     // Active high BRAM output register reset
assign ct_ena     = 1'b1;       // Internal BRAM clock gating disabled

///////////////////////////////////////////////////////
// Specify reset condition and next state transition //
///////////////////////////////////////////////////////

always @ (posedge clk) begin
    if (rst_b == 1'b0) begin
        current_state <= STATE_INIT;
    end else begin
        current_state <= next_state;
    end
end

///////////////////////////////
// Specify state transitions //
///////////////////////////////

always @ (*) begin

    // Default behaviour
    next_state = current_state;
 
    case (current_state)
 
        STATE_INIT: begin
            if (go_pulse == 1'b1) begin
                next_state = STATE_READ_BLOCK;   // "Go" condition
            end else begin 
                next_state = STATE_INIT;         // Else remain in INIT state
            end
        end
        
        STATE_READ_BLOCK: begin
            next_state = STATE_LOAD_BLOCK;       // Unconditional transition to LOAD_BLOCK state
        end
        
        STATE_LOAD_BLOCK: begin
            next_state = STATE_BUSY;             // Unconditional transition to BUSY state
        end
        
        STATE_BUSY: begin
            if (cnt == 5'b11111) begin
                next_state = STATE_WRITE_BLOCK;  // Move to WRITE_BLOCK state if 31 rounds elapsed
            end else begin
                next_state = STATE_BUSY;         // Else remain in BUSY state
            end
        end
        
        STATE_WRITE_BLOCK: begin
            if (current_block != num_blocks - 1) begin
                next_state = STATE_READ_BLOCK;   // Move to READ_BLOCK state - more blocks to be encrypted
            end else begin
                next_state = STATE_DONE;         // Move to DONE state - all blocks encrypted
            end
        end
        
        STATE_DONE: begin
            next_state = STATE_INIT;             // Unconditional transition to INIT state
        end
    
        STATE_RSVD_1: begin
            next_state = STATE_INIT;             // Unconditional transition to INIT state
        end
        
        STATE_RSVD_2: begin
            next_state = STATE_INIT;             // Unconditional transition to INIT state
        end
    
    endcase
end

//////////////////////////
// Specify output logic //
//////////////////////////
always @ (*) begin

    // Default assignments
    enable_counter   = 1'b0;
    core_load        = 1'b0;
    incr_block_count = 1'b0;
    done_intr        = 1'b0;
    
    pt_addra       = current_addr;
    ct_addra       = current_addr;
    ct_wr_data     = '0;
    ct_wea         = '0;
    
    case (current_state)
    
        STATE_INIT: begin
        end
    
        STATE_READ_BLOCK: begin
            pt_addra = current_addr;
        end

        STATE_LOAD_BLOCK: begin
            core_load = 1'b1;
        end

        STATE_BUSY: begin
            enable_counter = 1'b1;
        end

        STATE_WRITE_BLOCK: begin
            ct_addra         = current_addr;
            ct_wr_data       = core_ciphertext;
            ct_wea           = '1;
            incr_block_count = 1'b1;
        end
        
        STATE_DONE: begin
            done_intr = 1'b1;
        end
        
        STATE_RSVD_1: begin
        end
        
        STATE_RSVD_2: begin
        end
        
    endcase
end

///////////////////
// Round counter //
///////////////////

always @ (posedge clk) begin
    if (rst_b == 1'b0) begin
        cnt <= 5'b00001;
    end else if (enable_counter == 1'b0) begin
        cnt <= 5'b00001;
    end else if (enable_counter == 1'b1) begin
        cnt <= cnt + 1;
    end
end

////////////////////////////
// Current block register //
////////////////////////////

always @ (posedge clk) begin
    if ((rst_b == 1'b0) || (current_state == STATE_INIT)) begin
        current_block <= 11'b0;
        current_addr <= 'b0;
    end else if (incr_block_count) begin
        current_block <= current_block + 1;
        current_addr <= current_addr + 8;
    end
end

endmodule
