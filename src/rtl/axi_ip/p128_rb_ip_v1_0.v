
`timescale 1 ns / 1 ps
	
module p128_rb_ip_v1_0  #(

	parameter integer C_S00_AXI_DATA_WIDTH	= 32,
	parameter integer C_S00_AXI_ADDR_WIDTH	= 5

) (
	
    // Interface to Plaintext BRAM 
    output wire           pt_clka,
    output wire           pt_rsta,
    output wire [31:0]    pt_addra,
    output wire [63:0]    pt_wr_data,
    input  wire [63:0]    pt_rd_data,
    output wire           pt_ena,
    output wire [7:0]     pt_wea,
   
    // Interface to Ciphertext BRAM
    output wire           ct_clka,
    output wire           ct_rsta,
    output wire [31:0]    ct_addra,
    output wire [63:0]    ct_wr_data,
    input  wire [63:0]    ct_rd_data,
    output wire           ct_ena,
    output wire [7:0]     ct_wea,

    // AXI Slave interface
    input  wire                                    s00_axi_aclk,
    input  wire                                    s00_axi_aresetn,
    input  wire [(C_S00_AXI_ADDR_WIDTH-1):0]       s00_axi_awaddr,
    input  wire [2:0]                              s00_axi_awprot,
    input  wire                                    s00_axi_awvalid,
    output wire                                    s00_axi_awready,
    input  wire [(C_S00_AXI_DATA_WIDTH-1):0]       s00_axi_wdata,
    input  wire [((C_S00_AXI_DATA_WIDTH/8)-1):0]   s00_axi_wstrb,
    input  wire                                    s00_axi_wvalid,
    output wire                                    s00_axi_wready,
    output wire [1:0]                              s00_axi_bresp,
    output wire                                    s00_axi_bvalid,
    input  wire                                    s00_axi_bready,
    input  wire [(C_S00_AXI_ADDR_WIDTH-1):0]       s00_axi_araddr,
    input  wire [2:0]                              s00_axi_arprot,
    input  wire                                    s00_axi_arvalid,
    output wire                                    s00_axi_arready,
    output wire [(C_S00_AXI_DATA_WIDTH-1):0]       s00_axi_rdata,
    output wire [1:0]                              s00_axi_rresp,
    output wire                                    s00_axi_rvalid,
    input  wire                                    s00_axi_rready,

    // Edge triggered interrupt to ARM core
    output wire                                    p128_done_intr
);

// Connections from AXI slave interface to controller
wire [127:0]   p128_key;
wire           p128_go_pulse;
wire [10:0]    p128_num_blocks;
wire           p128_done_pulse;

// Connections from controller to cipher core
wire           core_load;
wire [63:0]    core_plaintext;
wire [127:0]   core_key;
wire [4:0]     core_count;
wire [63:0]    core_ciphertext;

//////////////////////////////////
// AXI slave interface instance //
//////////////////////////////////

p128_rb_ip_v1_0_S00_AXI # ( 
	.C_S_AXI_DATA_WIDTH  (C_S00_AXI_DATA_WIDTH),
	.C_S_AXI_ADDR_WIDTH  (C_S00_AXI_ADDR_WIDTH)
) p128_rb_ip_v1_0_S00_AXI_inst (
    .p128_key            (p128_key),
    .p128_go_pulse       (p128_go_pulse),
    .p128_num_blocks     (p128_num_blocks),
	.S_AXI_ACLK          (s00_axi_aclk),
	.S_AXI_ARESETN       (s00_axi_aresetn),
	.S_AXI_AWADDR        (s00_axi_awaddr),
	.S_AXI_AWPROT        (s00_axi_awprot),
	.S_AXI_AWVALID       (s00_axi_awvalid),
	.S_AXI_AWREADY       (s00_axi_awready),
	.S_AXI_WDATA         (s00_axi_wdata),
	.S_AXI_WSTRB         (s00_axi_wstrb),
	.S_AXI_WVALID        (s00_axi_wvalid),
	.S_AXI_WREADY        (s00_axi_wready),
	.S_AXI_BRESP         (s00_axi_bresp),
	.S_AXI_BVALID        (s00_axi_bvalid),
	.S_AXI_BREADY        (s00_axi_bready),
	.S_AXI_ARADDR        (s00_axi_araddr),
	.S_AXI_ARPROT        (s00_axi_arprot),
	.S_AXI_ARVALID       (s00_axi_arvalid),
	.S_AXI_ARREADY       (s00_axi_arready),
	.S_AXI_RDATA         (s00_axi_rdata),
	.S_AXI_RRESP         (s00_axi_rresp),
	.S_AXI_RVALID        (s00_axi_rvalid),
	.S_AXI_RREADY        (s00_axi_rready)
);

/////////////////////////////////////
// PRESENT-128 controller instance //
/////////////////////////////////////

p128_rb_ctrl i_p128_rb_ctrl (
    // System + register interface
    .clk              (s00_axi_aclk),
    .rst_b            (s00_axi_aresetn),
    .go_pulse         (p128_go_pulse),
    .num_blocks       (p128_num_blocks),
    .done_intr        (p128_done_intr),
    .key              (p128_key),
    // Plaintext block RAM interface
    .pt_clka          (pt_clka),
    .pt_rsta          (pt_rsta),
    .pt_addra         (pt_addra),
    .pt_wr_data       (pt_wr_data),
    .pt_rd_data       (pt_rd_data),
    .pt_ena           (pt_ena),
    .pt_wea           (pt_wea),
    
    // Ciphertext block RAM interface
    .ct_clka          (ct_clka),
    .ct_rsta          (ct_rsta),
    .ct_addra         (ct_addra),
    .ct_wr_data       (ct_wr_data),
    .ct_rd_data       (ct_rd_data),
    .ct_ena           (ct_ena),
    .ct_wea           (ct_wea),
    
    // Cipher core interface
    .core_load        (core_load),
    .core_plaintext   (core_plaintext),
    .core_key         (core_key),
    .core_count       (core_count),
    .core_ciphertext  (core_ciphertext)
);

//////////////////////////
// Cipher Core instance //
//////////////////////////

p128_rb_cipher_core i_p128_cipher_core (
    .clk              (s00_axi_aclk),
    .rst_b            (s00_axi_aresetn),       
    .load             (core_load),        
    .plaintext        (core_plaintext),   
    .key              (core_key),         
    .count            (core_count),       
    .ciphertext       (core_ciphertext)
);

endmodule
