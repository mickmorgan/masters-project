// Module: p128_pipe_cipher_core_tb
// Description : PRESENT-128 cipher core testbench (pipelined architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_pipe_cipher_core_tb (

);

// Declare variables
logic         clk;                // System clock
logic         rst_b;              // System reset (active low)
logic [63:0]  plaintext;          // 64-bit plaintext block
logic [127:0] key;                // 128-bit encryption key
logic [63:0]  ciphertext;         // 64-bit ciphertext block
logic         go;                 // Status flag to time readback

// Declare test vector arrays
localparam  NUM_TEST_VECTORS = 1;
reg [63:0]  plaintext_vectors  [0:(NUM_TEST_VECTORS-1)];
reg [127:0] key_vectors        [0:(NUM_TEST_VECTORS-1)];
reg [63:0]  ciphertext_vectors [0:(NUM_TEST_VECTORS-1)];

// Read test vectors from memory files
initial begin
    $display("Reading test vectors from memory files...");
    $readmemh("plaintext_vectors.mem", plaintext_vectors);
    $readmemh("key_vectors.mem", key_vectors);
    $readmemh("ciphertext_vectors.mem", ciphertext_vectors);
end

// Instantiate DUT (Design Under Test)    
p128_pipe_cipher_core i_dut (
    .clk (clk),
    .rst_b (rst_b),
    .plaintext (plaintext),
    .key (key),
    .ciphertext (ciphertext)
);

// Generate a clock
initial begin
    clk = 1'b0;
    forever #10 clk = ~clk;
end

// De-assert reset after delay (asynchronous to clock)
initial begin
    rst_b = 1'b0;
    #45
    rst_b = 1'b1;
end

// DUT driver
initial begin

    // Initialize flag and inputs
    go <= 1'b0;
    plaintext <= '0;
    key <= '0;
    
    
    // Wait for reset de-assertion
    @(posedge rst_b); 
    
    // Loop through each test vector and load on rising edge
    for (int i=0; i<NUM_TEST_VECTORS; i=i+1) begin
        @(posedge clk);
        go <= 1'b1;
        plaintext <= plaintext_vectors[i];
        key <= key_vectors[i];
    
    end
end

// DUT checker
initial begin

    // Wait for blocks to propagate through pipeline
    @(posedge go);
    for (int i=0; i<31; i=i+1) begin
        @(posedge clk);
    end
    
    // Read ciphertext blocks
    for (int i=0; i<NUM_TEST_VECTORS; i=i+1) begin
        @(posedge clk)
        $display("Test vector index %d", i);
        $display("    Plaintext          : %h", plaintext_vectors[i]);
        $display("    Key                : %h", key_vectors[i]);
        $display("    Expected ciphertext: %h", ciphertext_vectors[i]);
        $display("    Actual ciphertext  : %h", ciphertext);
        assert (ciphertext == ciphertext_vectors[i])
                    else $display("Test vector index %d failed due to expected data mismatch", i);
    end
    
    $finish;
end

endmodule
