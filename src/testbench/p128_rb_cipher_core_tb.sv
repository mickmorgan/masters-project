// Module: p128_rb_cipher_core_tb
// Description : PRESENT-128 cipher core testbench (round-based architecture)
// Author: Michael Morgan

`timescale 1ns / 1ps

module p128_rb_cipher_core_tb (

);

// Declare variables
logic         clk;                  // System clock
logic         rst_b;                // System reset (active low)
logic         load;                 // Load plaintext + key
logic [63:0]  plaintext;            // 64-bit plaintext block
logic [127:0] key;                  // 128-bit encryption key
logic [4:0]   count;                // 5-bit round counter
logic [63:0]  ciphertext;           // 64-bit ciphertext block

// Declare test vector arrays
localparam  NUM_TEST_VECTORS = 40;
reg [63:0]  plaintext_vectors  [0:(NUM_TEST_VECTORS-1)];
reg [127:0] key_vectors        [0:(NUM_TEST_VECTORS-1)];
reg [63:0]  ciphertext_vectors [0:(NUM_TEST_VECTORS-1)];

// Read test vectors from memory files
initial begin
    $display("Reading test vectors from memory files...");
    $readmemh("plaintext_vectors.mem", plaintext_vectors);
    $readmemh("key_vectors.mem", key_vectors);
    $readmemh("ciphertext_vectors.mem", ciphertext_vectors);
end

// Instantiate DUT (Design Under Test)    
p128_rb_cipher_core i_dut (
    .clk (clk),
    .rst_b (rst_b),
    .load (load),
    .plaintext (plaintext),
    .key (key),
    .count (count),
    .ciphertext (ciphertext)
);

// Generate a clock
initial begin
    clk = 1'b0;
    forever #10 clk = ~clk;
end

// De-assert reset after delay (asynchronous to clock)
initial begin
    rst_b = 1'b0;
    #45
    rst_b = 1'b1;
end

// Main stimulus
initial begin

    // Initialize load and count inputs
    load <= 1'b0;
    count <= 5'b00001;
    
    // Wait for reset de-assertion and next clock edge
    @(posedge rst_b); 
    @(posedge clk);
    
    // Loop through each test vector
    for (int i=0; i<NUM_TEST_VECTORS; i=i+1) begin

        $display("Test vector index %d", i);
        
        // Load plaintext and key test vectors
        $display("    Plaintext          : %h", plaintext_vectors[i]);
        $display("    Key                : %h", key_vectors[i]);
        plaintext <= plaintext_vectors[i];
        key <= key_vectors[i];
        load <= 1'b1;
        
        // De-assert load input and initialize count
        @(posedge clk);
        load <= 1'b0;
        count <= 5'b00001;
        
        // Increment count for 30 clock cycles
        for (int j=0; j<=31; j=j+1) begin
            @(posedge clk);
            count <= count + 1;
        end
        
        // Check output ciphertext matched expected value
        $display("    Expected ciphertext: %h", ciphertext_vectors[i]);
        $display("    Actual ciphertext  : %h", ciphertext);
        assert (ciphertext == ciphertext_vectors[i])
            else $display("Test vector index %d failed due to expected data mismatch", i);
        
    end

    $finish;
    
end

endmodule
